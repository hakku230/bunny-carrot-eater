import * as PIXI from "pixi.js";

import RabbitJSON from "./assets/textures/rabbit.json";
import FoxJSON from "./assets/textures/fox.json";

// Create our application instance
const app = new PIXI.Application({
    width: window.innerWidth,
    height: window.innerHeight,
    backgroundColor: 0x2c4e2c
});

document.body.appendChild(app.view);

const sheet_loader = (name, json) => {
    const sheet = new PIXI.Spritesheet(
        app.loader.resources[name].texture.baseTexture,
        json
    );
    sheet.parse((...args) => {});
    app.loader.resources[name].spritesheet = sheet
}

// Load the textures
app.loader.add('bunny',  require('./assets/textures/bunny.png'))
          .add('carrot', require('./assets/textures/carrot.png'))
		  .add('shit', require('./assets/textures/shit.png'))
          .add('flower1', require('./assets/textures/flower.png'))
          .add('flower2', require('./assets/textures/flower2.png'))
          .add('flower3', require('./assets/textures/flower3.png'))
          .add('flower4', require('./assets/textures/flower4.png'))
          .add('rabbit', require('./assets/textures/rabbit.png'))
          .add('fox', require('./assets/textures/fox.png'))
          .load((loader, resources) => {
            const sheets = [
                {name: 'rabbit', json: RabbitJSON},
                {name: 'fox', json: FoxJSON}
            ]

            sheets.forEach(sheetData => sheet_loader(sheetData.name, sheetData.json))
        });

export default app;