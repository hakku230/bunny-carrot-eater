import axios from 'axios'

const apiPort = process.env.PORT || 3001

var baseURL = location.origin + '/api';

if ( location.host.match('localhost') ) {
    baseURL = 'http://localhost:' + apiPort + '/api';
}

const api = axios.create({
    baseURL: baseURL,
})

export const insertScore = payload => api.post(`/score`, payload)
export const getAllScores = () => api.get(`/scores`)
export const updateScoreById = (id, payload) => api.put(`/score/${id}`, payload)
export const deleteScoreById = id => api.delete(`/score/${id}`)
export const getScoreById = id => api.get(`/score/${id}`)
export const getScoreByName = name => api.get(`/score/name/${name}`)

export const key = 'mBSYJyF3'

const apis = {
    insertScore,
    getAllScores,
    updateScoreById,
    deleteScoreById,
    getScoreById,
    getScoreByName,
    key
}

export default apis