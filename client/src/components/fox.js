import * as PIXI from "pixi.js";
import app from "../app";
import Unit from "./Unit";

export default class Fox extends Unit {
    _body
    _sprite
    speed = 4
    state = 'idle' // chase idle bite runaway
    timer = 600

    constructor() {
        super()

        this.position.set(-50, -50)

        this._sprite = new PIXI.AnimatedSprite(app.loader.resources.fox.spritesheet.animations["down"])
		this._sprite.anchor.set(0.5, 0.75);
		this._sprite.direction = 'down'
		this._sprite.animationSpeed = 0.1
		this._sprite.play()

		const body_size = 32
		this._body = new PIXI.Graphics()
		this._body.beginFill(0x000000, 0.5)
		this._body.drawRect(-body_size / 2, -body_size / 2, body_size, body_size)
		this._body.endFill()
		this._body.alpha = 0

		this.addChild(this._sprite, this._body)

		app.ticker.add(this.proceed.bind(this))
    }

    follow(bunny, delta) {
        let speed = this.speed * delta;

        this.moveTo(bunny, speed)

        const direction = Math.abs(this.y - bunny.y) >= Math.abs(this.x - bunny.x)
			? this.y - bunny.y <= 0 ? 'down' : 'up'
			: this.x - bunny.x < 0 ? 'right' : 'left'

		if ( direction !== this._sprite.direction ) {
			this._sprite.direction = direction

			this._sprite.textures = app.loader.resources.fox.spritesheet.animations[this._sprite.direction]
			this._sprite.play()
		}
    }

    runaway(delta) {
        let speed = this.speed * delta;

        const points = [
            {x: -50, y: -50},
            {x: window.innerWidth + 50, y: -50},
            {x: -50, y: window.innerHeight + 50},
            {x: window.innerWidth + 50, y: window.innerHeight + 50}
        ]

        const nearestPoint = points.sort((a, b) => {return (Math.abs(a.x - this.x) + Math.abs(a.y - this.y)) < (Math.abs(b.x - this.x) + Math.abs(b.y - this.y)) ? -1 : 1})[0]

        this.moveTo(nearestPoint, speed)

        if ( this.x === nearestPoint.x && this.y === nearestPoint.y ) {
            this.state = 'idle'
            this.timer = 600
        }

        const direction = Math.abs(this.y - nearestPoint.y) >= Math.abs(this.x - nearestPoint.x)
			? this.y - nearestPoint.y <= 0 ? 'down' : 'up'
			: this.x - nearestPoint.x < 0 ? 'right' : 'left'

		if ( direction !== this._sprite.direction ) {
			this._sprite.direction = direction

			this._sprite.textures = app.loader.resources.fox.spritesheet.animations[this._sprite.direction]
			this._sprite.play()
		}
    }

    proceed(delta) {
        if (this.timer > 0) this.timer -= delta

        if (this.timer <= 0) switch(this.state) {
            case 'idle': {
                this.timer = 600
                this.state = 'chase'
                break
            }
            case 'chase': {
                this.state = 'runaway'
                break
            }
            case 'bite': {
                this.timer = 600
                this.state = 'chase'
                break
            }
            default: break;
        }
    }
}