import * as PIXI from "pixi.js";

import app from "../app.js";

export default class Carrot {
	_sprite;

	constructor() {
		let carrot =  new PIXI.Sprite(app.loader.resources.carrot.texture);
		carrot.position = {
            x: getRandomInt(app.renderer.width),
            y: getRandomInt(app.renderer.height)
        };
		carrot.anchor.set(0.5);
		carrot.width = 32;
		carrot.height = 32;

		this._sprite = carrot;
	}

	getSprite() {
		return this._sprite;
	}

	getBody() {
		return this._sprite;
	}

	setCoordinates(point, random = false) {
		if (random) {
			this._sprite.position.set(
				Math.max(0, Math.min(window.innerWidth, point.x + (Math.floor(Math.random() * 2) - 1) * window.innerWidth / 10)),
				Math.max(0, Math.min(window.innerHeight, point.y + (Math.floor(Math.random() * 2) - 1) * window.innerHeight / 10))
			)
		} else {
			this._sprite.position = point
		}
	}
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}