import * as PIXI from "pixi.js";

import app from "../app.js";
import Carrot from "./carrot.js";

export default class Flower {
	_sprite;

	bloomOptions

	constructor(bloomChance = 0.5) {
		let flower =  new PIXI.Sprite(app.loader.resources['flower' + Math.floor(Math.random() * 4 + 1)].texture);

		flower.anchor.set(0.5);
		flower.width = 32;
		flower.height = 32;

		this._sprite = flower;

		this.bloomOptions = {
			rate: 60 * (Math.round(Math.random() * 8) + 5),
			counter: 0,
			chance: bloomChance
		}
	}

	getSprite() {
		return this._sprite;
	}

	getBody() {
		return this._sprite;
	}

	setCoordinates(point, random = false) {
		if (random) {
			this._sprite.position.set(
				Math.max(0, Math.min(window.innerWidth, point.x + ((Math.random() * 2) - 1) * window.innerWidth / 10)),
				Math.max(0, Math.min(window.innerHeight, point.y + ((Math.random() * 2) - 1) * window.innerHeight / 10))
			)
		} else {
			this._sprite.position = point
		}
	}

	bloom(delta) {
		const outcome = {
			flowers: [],
			carrots: []
		}

		if ( this.bloomOptions.counter <= 0 ) {
            this.bloomOptions.counter = this.bloomOptions.rate

            if ( Math.random() < this.bloomOptions.chance ) {
				this.bloomOptions.chance /= 2

				if (Math.random() > 0.25) {
					let flower = new Flower(this.bloomOptions.chance);
					flower.setCoordinates(this._sprite.position, true);
					outcome.flowers.push(flower)
				} else {
					let carrot = new Carrot();
					carrot.setCoordinates(this._sprite.position, true);
					outcome.carrots.push(carrot)
				}
			}
        }

		this.bloomOptions.counter -= delta

		return outcome;
	}
}