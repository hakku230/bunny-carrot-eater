import * as PIXI from "pixi.js";

import app from "../app.js";

import Flower from "./flower.js"
import Carrot from "./carrot.js"

export default class Shit {
	_sprite;

	constructor() {
		let shit =  new PIXI.Sprite(app.loader.resources.shit.texture);

		shit.anchor.set(0.5);
		shit.width = 16;
		shit.height = 16;

		this.init();

		this._sprite = shit;
	}

	init() {
		let shit = this;

		shit.eatable = false;
		shit.fertilized = false;

		let time = 0

		const proceed = (delta) => {
			time += delta

			if ( time >= 60 ) { // 1000
				shit.eatable = true
			}

			if ( time >= 600 ) { // 10000
				shit.fertilized = true

				app.ticker.remove(proceed)
			}
		}

		app.ticker.add(proceed)
	}

	getSprite() {
		return this._sprite;
	}

	getBody() {
		return this._sprite;
	}

	setCoordinates(point) {
		this._sprite.position = point
	}

	setSize(width, height) {
		this._sprite.width = width;
    	this._sprite.height = height;
	}

	fertilize() {
		const outcome = {
			flowers: [],
			carrots: []
		}

		const power = Math.round(this._sprite.width / 16)

		for (let i = 0; i < power; i++) {
			if (Math.random() > 0.25) {
				let flower = new Flower();
				flower.setCoordinates(this._sprite.position, power > 1);
				outcome.flowers.push(flower)
			} else {
				let carrot = new Carrot();
				carrot.setCoordinates(this._sprite.position, power > 1);
				outcome.carrots.push(carrot)
			}
		}

		return outcome;
	}
}