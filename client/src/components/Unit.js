import * as PIXI from "pixi.js";

export default class Unit extends PIXI.Container {
    _body
    _sprite

    constructor() {
        super()
    }

    moveTo(object, speed) {
        const side1 = Math.abs(object.x - this.x)
        const side2 = Math.abs(object.y - this.y)

        const vector = Math.sqrt(side1 * side1 + side2 * side2)

        if ( Math.abs(object.x - this.x) < speed ) {
			this.x = object.x
        } else {
        	let point = (object.x - this.x) / Math.abs(object.x - this.x)
        	this.x += point * (side1 / vector) * speed
        }

        if ( Math.abs(object.y - this.y) < speed ) {
			this.y = object.y
        } else {
        	let point = (object.y - this.y) / Math.abs(object.y - this.y)
        	this.y += point * (side2 / vector) * speed
        }
    }

	getBody() {
		return this._body;
	}

    getSprite() {
		return this._sprite;
	}
}