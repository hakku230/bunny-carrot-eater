import sounds from 'pixi-sound';

sounds.sound.add('eated', require("url:../assets/sounds/eated.mp3"));
sounds.sound.add('hungry', require("url:../assets/sounds/hungry.mp3"));
sounds.sound.add('died', require("url:../assets/sounds/died.mp3"));
sounds.sound.add('flower', require("url:../assets/sounds/flower.mp3"));
sounds.sound.add('makeShit', require("url:../assets/sounds/makeShit.mp3"));
sounds.sound.add('stepShit', require("url:../assets/sounds/stepShit.mp3"));
sounds.sound.add('bite', require("url:../assets/sounds/roar.mp3"));

let soundSwitcher = document.querySelector('.sound-switcher')

soundSwitcher.addEventListener('click', () => {
    if ( soundSwitcher.classList.contains('on') ) {
        soundSwitcher.classList.remove('on')
        sounds.volumeAll = 0
    } else {
        soundSwitcher.classList.add('on')
        sounds.volumeAll = 1
    }
})

export default sounds;