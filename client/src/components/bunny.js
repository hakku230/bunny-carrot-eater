import * as PIXI from "pixi.js";
import Unit from "./Unit";

import app from "../app.js";
import sounds from "./sounds.js"

import Shit from "./shit.js"

export default class Bunny extends Unit {
	_body;
	_sprite;
	_basicSpeed = 5;
	eatedCarrots = 0;
	slowDownTimer = 0;
	slowed = false;
	health = 100;
	_onHealthChangeCallback;
	underFlower = {
		active: false,
		bonus: 2,
		delay: 0
	};
	restBonus = 0.67;
	growth = 1;
	basicScale = {
		x: 1,
		y: 1
	}
	hungryNoise = false;
	followedPosition = false;

	constructor() {
		super()

		this._sprite = new PIXI.AnimatedSprite(app.loader.resources.rabbit.spritesheet.animations["idle/down"])
		this._sprite.scale = this.basicScale;
		this._sprite.anchor.set(0.5);
		this._sprite.type = 'idle'
		this._sprite.direction = 'down'
		this._sprite.animationSpeed = 0.1
		this._sprite.play()

		// Move the sprite to the center of the screen
	    this.position = this.followedPosition = {
	    	x: app.renderer.width / 2,
	    	y: app.renderer.height / 2
		};

		const body_size = 32
		this._body = new PIXI.Graphics()
		this._body.beginFill(0x000000, 0.5)
		this._body.drawRect(-body_size / 2, -body_size / 2, body_size, body_size)
		this._body.endFill()
		this._body.alpha = 0

		this.addChild(this._sprite, this._body)

		app.ticker.add(this.proceed.bind(this))
	}

	isDead() {
		return this.health === 0;
	}


	getHungry(mouseposition, delta) {
		let hungryLevel = -0.1 * this.growth / this.underFlower.bonus * delta;
		if ( !this.isMoving(mouseposition) ) hungryLevel *= this.restBonus;

		this.changeHealth( hungryLevel );
	}

	animate(mouseposition, delta) {
		const type = this.isMoving(mouseposition) ? 'move' : 'idle'

		const direction = Math.abs(this.y - mouseposition.y) >= Math.abs(this.x - mouseposition.x)
			? this.y - mouseposition.y <= 0 ? 'down' : 'up'
			: this.x - mouseposition.x < 0 ? 'right' : 'left'

		if ( type !== this._sprite.type || direction !== this._sprite.direction ) {
			this._sprite.type = type
			this._sprite.direction = direction

			this._sprite.textures = app.loader.resources.rabbit.spritesheet.animations[this._sprite.type + "/" + this._sprite.direction]
			this._sprite.play()
		}
	}

	changeHealth(change) {
		let health = this.health + change;
		let exceed = 0;

		if ( this.hungryNoise && health < 25 ) {
			this.hungryNoise = false;
			sounds.sound.play('hungry');
		}

		if ( health > 40 ) {
			this.hungryNoise = true;
		}

		if ( health < 0 ) health = 0;
		if ( health > 100 ) {
			exceed = health - 100;
			health = 100;
		}

		if ( health === 0 ) {
			sounds.sound.play('died');
		}

		this.health = health;
		this._onHealthChangeCallback(health);

		return exceed;
	}

	setHealthChangeCallback(callback) {
		this._onHealthChangeCallback = callback;
	}

	setSpeed(speed) {
		this._basicSpeed = speed;
	}

	isMoving(mouseposition) {
		return !( this.x == mouseposition.x && this.y == mouseposition.y );
	}

	followTheMouse(mouseposition, delta) {
		let speed = this._basicSpeed * delta;

		this.moveTo(mouseposition, speed)
	}

	followThePosition(e) {
		return this.followedPosition = e.data.global
	}

	endFollowing(e) {
		return this.followedPosition = this._sprite.position
	}

	grow() {
		this.scale.x = this.basicScale.x * this.growth;
        this.scale.y = this.basicScale.y * this.growth;
	}

	feedCarrot(counter) {
		sounds.sound.play('eated');

		const exceed = this.changeHealth(10 / this.growth);
		this.eatedCarrots += 1;

		this.growth += Math.max(exceed / 100, 0.01); // 0.05
		this.grow();

        counter.up();
	}

	feedFlower() {
		sounds.sound.play('flower');

		this.underFlower.delay = 60 * 2 // 2000

		if ( this.underFlower.active === false ) {
			this.underFlower.active = true;
			this.setSpeed(this._basicSpeed * this.underFlower.bonus);
		}
	}

	proceed(delta) {
		if (this.underFlower.delay > 0) this.underFlower.delay -= delta
		if (this.slowDownTimer > 0) this.slowDownTimer -= delta

		if (this.underFlower.active && this.underFlower.delay < 0) {
			this.setSpeed(this._basicSpeed / this.underFlower.bonus);
			this.underFlower.active = false;
		}

		if (this.slowed && this.slowDownTimer < 0) {
			this.setSpeed(this._basicSpeed * 5);
            this.slowed = false;
		}
	}

	slowDown() {
		sounds.sound.play('stepShit');

		this.slowDownTimer = 60 * 2 // 2000

		if ( !this.slowed ) {
			this.setSpeed(this._basicSpeed / 5);
			this.slowed = true;
		}
	}

	makeShit() {
		sounds.sound.play('makeShit');

		let shit = new Shit();
		shit.setCoordinates(this.position);
		shit.setSize( 16 * this.growth, 16 * this.growth );

		this.eatedCarrots -= Math.floor( 5 * this.growth );

		this.growth = Math.max(1, 1 + (this.growth - 1) * 0.8); // 20% loss
		this.grow();

		return shit;
	}
}