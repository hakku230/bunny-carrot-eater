import api from '../api'

var sha256 = require('js-sha256').sha256;

class ApiController {
    constructor() {}

    showTopTen() {
        api.getAllScores().then(response => {
            let popup = document.querySelector('.popup-highscores');
            let scoresList = popup.querySelector('.highscores');
        
            if ( response.status === 200 ) {
                let scores = response.data.data;
        
                scores.forEach(score => {
                    scoresList.innerHTML += '<li class="highscore"><span class="highscore-name">' + score.name + '</span><span class="highscore-score">' + score.score + '</span></li>';
                });
            }
        }, response => {
            let popup = document.querySelector('.popup-highscores');
            let errorMessage = popup.querySelector('.error');
        
            errorMessage.textContent = 'Sorry, no highscores were found :(';
        })
    }

    saveResult(count) {
        let popup = document.querySelector('.popup-endgame');
        let form = popup.querySelector('.record-score');

        let name = form.querySelector('input[name="name"]').value;

        api.getScoreByName(name).then(response => {
            let lastScore = response.data.data.score;

            if ( lastScore < count ) {
                if ( response.status === 200 ) {
                    // update
                    api.updateScoreById(response.data.data._id, {
                        name: name,
                        score: count,
                        hash: sha256.hmac(api.key, response.data.data._id + name + count)
                    }).then(response => {
                        popup.querySelector('.message').textContent = 'Your last score is: ' + lastScore;
                        form.remove();
                    });
                }
            } else {
                popup.querySelector('.message').textContent = 'Your last score is: ' + lastScore;
                form.remove();
            }
        }, response => {
            // insert
            api.insertScore({
                name: name,
                score: count,
                hash: sha256.hmac(api.key, name + count)
            }).then(response => {
                popup.querySelector('.message').textContent = 'Your first score was saved';
                form.remove();
            });
        })
    }
}

let apiController = new ApiController()

export default apiController