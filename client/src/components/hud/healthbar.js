import * as PIXI from "pixi.js";

import app from "../../app.js";

class Bar {
	graphics;
	position;
	size;
	border;
	color;
	stage;
	percentage = 100;

	constructor(options) {
		let defaults = {
			position: {
				x: app.renderer.width - 320,
				y: app.renderer.height - 40
			},
			size: {
				width: 300,
				height: 20
			},
			border: 2,
			color: {
				inner: 0xDE3249,
				outer: 0x650A5A
			}
		}

		if ( typeof( options.stage ) === undefined  ) {
			console.error('No stage for Bar!');
			return false;
		}

		options = {...defaults, ...options};

		this.position = options.position;
		this.size = options.size;
		this.border = options.border;
		this.color = options.color;
		this.stage = options.stage;

		this.createGraphics();
	}

	createGraphics() {
		let graphics = new PIXI.Graphics();
		// Rectangle
		graphics.beginFill(this.color.outer);
		graphics.drawRect(
			this.position.x,
			this.position.y,
			this.size.width,
			this.size.height
		);
		graphics.endFill();

		// Rectangle
		graphics.beginFill(this.color.inner);
		graphics.drawRect(
			this.position.x + this.border ,
			this.position.y + this.border,
			(this.size.width - this.border * 2) * this.percentage / 100,
			this.size.height - this.border * 2
		);
		graphics.endFill();

		this.graphics = graphics;
	}

	draw() {
		this.stage.addChild( this.graphics );
	}

	setPercentage(percentage) {
		if ( !(percentage >= 0 && percentage <= 100) ) return this;
		this.graphics.destroy();
		this.percentage = percentage;

		this.createGraphics();

		this.draw();
	}
}

class HealthBar extends Bar {
	constructor(options) {
		super(options);
	}
}

let healthbar = new HealthBar({stage: app.stage});

healthbar.draw();

export default healthbar;