import counter from "./counter.js"
import healthbar from "./healthbar.js"

class Hud {
    constructor() {
        this.counter = counter
        this.healthbar = healthbar
    }
}

let hud = new Hud()

export default hud