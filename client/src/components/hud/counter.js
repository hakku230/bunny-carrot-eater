import * as PIXI from "pixi.js";

import app from "../../app.js";

class Counter {
	count = 0;
	_text;

	constructor() {
		const style = new PIXI.TextStyle({
		    fontFamily: 'Arial',
		    fontSize: 36,
		    fontStyle: 'italic',
		    fontWeight: 'bold',
		    fill: ['#ffffff', '#00ff99'], // gradient
		    stroke: '#4a1850',
		    strokeThickness: 5,
		    dropShadow: true,
		    dropShadowColor: '#000000',
		    dropShadowBlur: 4,
		    dropShadowAngle: Math.PI / 6,
		    dropShadowDistance: 6,
		    wordWrap: true,
		    wordWrapWidth: 440,
		});

		let counter = new PIXI.Text( this.count, style );

		counter.x = app.renderer.width / 2;
		counter.y = 100;

		this._text = counter;

		app.stage.addChild(counter);
	}

	up() {
		this.count ++;
		this._text.text = this.count;
	}
}

const counter = new Counter();

export default counter;