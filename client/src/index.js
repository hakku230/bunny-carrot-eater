import app from "./app.js";
import ApiController from "./components/ApiController.js"
import hud from "./components/hud"
import Bunny from "./components/bunny.js"
import Carrot from "./components/carrot.js"
import Fox from "./components/fox.js";
import sounds from "./components/sounds.js";

const baseCarrotsCoefficient = 0.000004198410146045895;

window.isStartScreen = true;

ApiController.showTopTen()

app.loader.onComplete.add(() => {new Game()});
app.loader.load();

// Save result button listener
(function() {
    let popup = document.querySelector('.popup-endgame');
    let form = popup.querySelector('.record-score');

    form.querySelector('button').onclick = function(e) {
        e.preventDefault();

        ApiController.saveResult(hud.counter.count)
    }
})()

class Game {
    bunny
    fox
    carrots = []
    shits = []
    flowers = []
    time = 0
    baseCarrots = 0

    constructor() {
        // Initialize bunny
        this.bunny = new Bunny();
        this.bunny.setHealthChangeCallback( health => {
            hud.healthbar.setPercentage( health );
        } );
        app.stage.addChild(this.bunny);

        // Initialize fox
        this.fox = new Fox();
        app.stage.addChild(this.fox);

        // Initialize starting carrots
        this.baseCarrots = Math.round( window.innerHeight * window.innerWidth * baseCarrotsCoefficient );
        for ( let count = 5; count > 0; count-- ) {
            let carrot = new Carrot();

            this.carrots.push(carrot);

            app.stage.addChild(carrot.getSprite());
        }

        // Setup controls
        app.stage.interactive = true
        app.renderer.plugins.interaction.on('pointermove', this.bunny.followThePosition.bind(this.bunny))
        app.renderer.plugins.interaction.on('touchstart', this.bunny.followThePosition.bind(this.bunny));
        app.renderer.plugins.interaction.on('touchmove', this.bunny.followThePosition.bind(this.bunny));
        app.renderer.plugins.interaction.on('touchcancel', this.bunny.endFollowing.bind(this.bunny));
        app.renderer.plugins.interaction.on('touchend', this.bunny.endFollowing.bind(this.bunny));

        // Listen for animate update
        app.ticker.add(this.gameLoop.bind(this));
    }

    bloom(delta) {
        this.flowers.forEach(flower => {
            const outcome = flower.bloom(delta)
            
            this.flowers.push(...outcome.flowers);
            this.carrots.push(...outcome.carrots);

            outcome.flowers.forEach(flower => app.stage.addChild(flower.getSprite()))
            outcome.carrots.forEach(carrot => app.stage.addChild(carrot.getSprite()))
        })
    }

    gameLoop(delta) {
        if ( isStartScreen ) {
            return;
        }
        
        if ( this.bunny.isDead() ) {
            let popup = document.querySelector('.popup-endgame');
    
            if ( popup.classList.contains('js-hide') ) {
                popup.classList.remove('js-hide');
    
                let form = popup.querySelector('.record-score');
                form.querySelector('.score').textContent = hud.counter.count;
            }
    
            return;
        }

        this.bloom(delta)

        if (this.fox.state === 'chase') this.fox.follow(this.bunny, delta)
        if (this.fox.state === 'runaway') this.fox.runaway(delta)
    
        this.bunny.followTheMouse(this.bunny.followedPosition, delta);
        this.bunny.getHungry(this.bunny.followedPosition, delta);
        this.bunny.animate(this.bunny.followedPosition, delta);
    
        if (this.bunny.eatedCarrots > Math.floor( 5 * this.bunny.growth ) ) {
            let shit = this.bunny.makeShit();
    
            this.shits.push(shit);
    
            app.stage.addChild(shit.getSprite());
        }
    
        this.time += 1 * delta;
        if ( this.time > ( 70 + getRandomInt(140) ) ) {
            let carrot = new Carrot();
    
            this.carrots.push(carrot);
    
            app.stage.addChild(carrot.getSprite());
    
            this.time = 0;
        }
    
        for( let key in this.shits ) {
            if ( this.shits[key].fertilized ) {
                const outcome = this.shits[key].fertilize();
    
                this.flowers.push(...outcome.flowers);
                this.carrots.push(...outcome.carrots);

                outcome.flowers.forEach(flower => app.stage.addChild(flower.getSprite()))
                outcome.carrots.forEach(carrot => app.stage.addChild(carrot.getSprite()))

                this.shits[key].getSprite().destroy();
                this.shits.splice(key, 1);
            }
        }
    
        checkCollisionInArrayWithItem(this.carrots, this.bunny, (function (key) {
            this.carrots[key].getSprite().destroy();
            this.carrots.splice(key, 1);
    
            this.bunny.feedCarrot(hud.counter);
    
            if ( this.carrots.length < this.baseCarrots ) {
                let delay = getRandomInt(60 / 6) * 6

                const spawn_carrot = (delta) => {
                    delay -= delta

                    if ( delay < 0 ) {
                        const carrot = new Carrot();
    
                        this.carrots.push(carrot);
        
                        app.stage.addChild(carrot.getSprite());
                        app.ticker.remove(spawn_carrot)
                    }
                }

                app.ticker.add(spawn_carrot)
            }
        }).bind(this));
    
        checkCollisionInArrayWithItem(this.shits, this.bunny, (function (key) {
            if ( this.shits[key].eatable ) {
                this.shits[key].getSprite().destroy();
                this.shits.splice(key, 1);
    
                this.bunny.slowDown();
            }
        }).bind(this));
    
        checkCollisionInArrayWithItem(this.flowers, this.bunny, (function (key) {
            this.flowers[key].getSprite().destroy();
            this.flowers.splice(key, 1);
    
            this.bunny.feedFlower();
            this.bunny.changeHealth(1 * delta);
        }).bind(this));

        checkCollisionBetweenItems(this.bunny, this.fox, (function () {
            if (this.fox.state === 'chase') {
                this.fox.timer = 30
                this.fox.state = 'bite'
                this.bunny.changeHealth(-10 / this.bunny.growth)
                sounds.sound.play('bite')
            }
        }).bind(this))

        checkCollisionInArrayWithItem(this.shits, this.fox, (function (key) {
            if ( this.shits[key] ) {
                this.shits[key].getSprite().destroy();
                this.shits.splice(key, 1);
    
                this.fox.state = 'runaway'
                sounds.sound.play('stepShit');
            }
        }).bind(this));
    }
}

function checkCollisionInArrayWithItem(inArray, withItem, callback) {
    for( let key in inArray ) {
        if ( boxesIntersect( inArray[key].getBody(), withItem.getBody() ) ) callback(key);
    }
}

function checkCollisionBetweenItems(item1, item2, callback) {
    if ( boxesIntersect( item1.getBody(), item2.getBody() ) ) callback();
}

function boxesIntersect(a, b) {
    var ab = a.getBounds();
    var bb = b.getBounds();
    return ab.x + ab.width > bb.x && ab.x < bb.x + bb.width && ab.y + ab.height > bb.y && ab.y < bb.y + bb.height;
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}