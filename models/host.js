const {Schema, model, Types} = require('mongoose')

const schema = new Schema({
    hostname: {type: String, required: true, unique: true},
})

module.exports = model('Host', schema)
