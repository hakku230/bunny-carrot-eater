const {Schema, model, Types} = require('mongoose')

const schema = new Schema({
    name: {type: String, required: true, unique: true},
    score: {type: Number, default: 0},
})

module.exports = model('Highscore', schema)
