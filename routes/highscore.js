const express = require('express')

const ScoreCtrl = require('../controllers/highscore')
const HostsCtrl = require('../controllers/host')

const router = express.Router()

function authorizedHost(req, res, next) {
    HostsCtrl.getHosts(req, res).then(response => {
        if ( req.authorizedHosts.success === true ) {
            let authorized = false;

            req.authorizedHosts.data.forEach(element => {
                if ( element.hostname == req.hostname ) authorized = true;
            });

            if ( authorized ) {
                next()
            }
            else {
                return res.status(401).json({ success: false, error: 'Unauthorized request' })
            }
        } else {
            return res.status(401).json({ success: false, error: 'Unauthorized request' })
        }
    }, response => {
        return res.status(401).json({ success: false, error: 'Unauthorized request' })
    })
}

router.post('/score', authorizedHost, ScoreCtrl.createScore)
router.put('/score/:id', authorizedHost, ScoreCtrl.updateScore)
router.delete('/score/:id', authorizedHost, ScoreCtrl.deleteScore)
router.get('/score/:id', authorizedHost, ScoreCtrl.getScoreById)
router.get('/score/name/:name', authorizedHost, ScoreCtrl.getScoreByName)
router.get('/scores', ScoreCtrl.getScores)

module.exports = router