var sha256 = require('js-sha256').sha256;

const Score = require('../models/highscore')

createScore = (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a score',
        })
    }

    let hash = sha256.hmac('mBSYJyF3', body.name + body.score);

    if ( hash != body.hash ) {
        return res.status(401).json({ success: false, error: 'Unauthorized request' })
    }

    const score = new Score()

    score.name = body.name
    score.score = body.score

    if (!score) {
        return res.status(400).json({ success: false, error: err })
    }

    score
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: score._id,
                message: 'Score created!',
            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'Score not created!',
            })
        })
}

updateScore = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    let hash = sha256.hmac('mBSYJyF3', req.params.id + body.name + body.score);

    if ( hash != body.hash ) {
        return res.status(401).json({ success: false, error: 'Unauthorized request' })
    }

    Score.findOne({ _id: req.params.id }, (err, score) => {
        if (err) {
            return res.status(404).json({
                err,
                message: 'Score not found!',
            })
        }
        score.name = body.name
        score.score = body.score

        score
            .save()
            .then(() => {
                return res.status(200).json({
                    success: true,
                    id: score._id,
                    message: 'Score updated!',
                })
            })
            .catch(error => {
                return res.status(404).json({
                    error,
                    message: 'Score not updated!',
                })
            })
    })
}

deleteScore = async (req, res) => {
    const body = req.body

    let hash = sha256.hmac('mBSYJyF3', req.params.id);

    if ( hash != body.hash ) {
        return res.status(501).json({ success: false, error: 'Not Implemented' })
    }

    await Score.findOneAndDelete({ _id: req.params.id }, (err, score) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!score) {
            return res
                .status(404)
                .json({ success: false, error: `Score not found` })
        }

        return res.status(200).json({ success: true, data: score })
    }).catch(err => console.log(err))
}

getScoreById = async (req, res) => {
    await Score.findOne({ _id: req.params.id }, (err, score) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!score) {
            return res
                .status(404)
                .json({ success: false, error: `Score not found` })
        }
        return res.status(200).json({ success: true, data: score })
    }).catch(err => console.log(err))
}

getScoreByName = async (req, res) => {
    await Score.findOne({ name: req.params.name }, (err, score) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!score) {
            return res
                .status(404)
                .json({ success: false, error: `Score not found` })
        }
        return res.status(200).json({ success: true, data: score })
    }).catch(err => console.log(err))
}

getScores = async (req, res) => {
    await Score.find({}, null, {'limit': 10, 'sort': '-score'}, (err, scores) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!scores.length) {
            return res
                .status(404)
                .json({ success: false, error: `Score not found` })
        }
        return res.status(200).json({ success: true, data: scores })
    }).catch(err => console.log(err))
}

module.exports = {
    createScore,
    updateScore,
    deleteScore,
    getScores,
    getScoreById,
    getScoreByName,
}