const Host = require('../models/host')

getHosts = async (req, res) => {
    await Host.find({}, (err, hosts) => {
        if (err) {
            return req.authorizedHosts = { success: false, error: err }
        }
        if (!hosts.length) {
            return req.authorizedHosts = { success: false, error: `Hosts not found` }
        }

        return req.authorizedHosts = { success: true, data: hosts }
    }).catch(err => { return req.authorizedHosts = { success: false, error: err } })
}

module.exports = {
    getHosts
}